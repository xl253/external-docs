# Glossary

This page contains a listing of some commonly confusing words, along with any helpful references for understanding what something is. Tech is filled with crazy jargon that is hard to keep up with. Don't get discouraged, and remember that >50% of it has been made up by some random person within the last 10 years.

## <a name="api"></a> API

API is a fun, ambiguous term. It stands for 'Application Programming Interface'. It's a fancy phrase for talking about some agreed-upon way for two programs to talk to each other. It can refer to the specification, as well as the implementation. In recent years, people have often meant 'HTTP API' (or API that uses http as the channel of communication) when talking about APIs. When someone says 'Twitter has an API', they mean Twitter has an _HTTP_ API, which boils down to a special webpage that works as a function. For example, it could be something like `twitter.com/api/tweets`, which would then respond with a list of all tweets in json format. APIs each have different ways of handling authentication. If you want to see an example of an HTTP api, check out the Co-lab's [interactive api documentation page](https://apidocs.colab.duke.edu).

## <a name="ci"></a> CI 

CI stands for Continuous Integration. It's a practice where you automate the steps of building your code into its executable form, automate its testing, and/or automate its deployment. Special CI sofware is usually used, which detects when you commit changes to your code

## <a name="commit"></a> Commit

'Committing' your code is something done when you are using git as your Source Code Manager. It's like hitting 'freeze' on your code, and making a backup, with a message to help you remember what you did.This way, you can document every change you make to your code, roll it back as necessary, or merge a bunch of changes you and others have made in parallel. A single 'commit' is the frozen version of your code. 

## <a name="deployment"></a> Deployment

Deployment is the process of taking the code that you've written, and telling it to run wherever it will be run. If you're making a standalone app, that might be posting it to the appstore. If you're building a web application, that might mean putting your code on a server and telling it to start. Every project has a different set of steps to deploy, and you're the one deciding what those will be. Check out CI for more info on some ways of deploying your code.

## <a name="git"></a> Git

Git is a piece of software called a Source Control Manager, that helps you keep track of changes you make to your code, and keep it all in one place. You can create what's called a 'git repository', which is just a folder where all your code resides. You can then tell git to freeze all the code in that folder, remember it exactly as it is, and push that frozen state to a central repository (like on github or gitlab), while you keep working on it. Others can collaborate on your project by pulling those frozen states ([commits](#commit)), making their own changes, and sending them back to the central repository. 

## <a name="html"></a> HTML

HTML stands for HyperText Markup Language. It's a way of placing and describing content on a webpage. (Almost) Every link, image, and piece of text you see on the web is written in html. Even this page is (after it's converted from markdown). You can see the html source of a page (or a piece of a page) simply by right-clicking the page and hitting 'inspect element', which will bring up the developer console for your web browser, letting you see the html that makes up your page. 

Of the three browser languages (HTML, CSS, and JavaScript), HTML is the one that embodies MEANING, describing and placing content on the page. CSS deals with positioning and appearance, while Javascript deals with reacting to user inputs and doing things that HTML and CSS cannot.

HTML example below:

```html
<html>
<head>
    <title>my news article site</title>
    <script src="myJS.js"></script>
    <link rel="stylesheet" href="mycss.css">
</head>
<body>
    <header>
        <h1>Welcome to My News Site</h1>
    </header>
    <main>
        <article>
            <h2>article 1</h2>
            <section>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam delectus culpa nihil, laudantium dolore laborum obcaecati, vero minima aspernatur, asperiores magni maxime quam non blanditiis voluptates fugit sequi quia ullam.</section>
            <section>Eum tempore illum repellat odit, nam aliquid porro veritatis voluptatem enim alias. Harum et molestiae quas, eum quia ea. Aut officia, magni in. Sint beatae facilis eveniet nihil, rem totam.</section>
            <section>Ratione aliquid fugiat adipisci, explicabo et quaerat, nisi laudantium ea soluta voluptatibus recusandae tempora. At quidem veniam, earum, omnis repellat iste nesciunt laborum officiis ullam iure sed, corporis ratione eum.</section>
        </article>
        <article>
            <h2>article 2</h2>
            <section>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae, unde, ratione odio repudiandae porro ipsa, quisquam ut dolorem sint tempore dignissimos veritatis hic provident distinctio nihil. Maiores delectus, sapiente illum.</section>
            <section>Soluta amet, aut est quas, ut nostrum iure alias sint corporis, fugit illo temporibus magnam error. Reprehenderit in, blanditiis id neque at pariatur distinctio beatae sapiente itaque veniam vel quisquam.</section>
            <section>Molestiae velit itaque laudantium dolore tenetur, consectetur hic, sequi modi asperiores expedita facere nesciunt. Id amet perspiciatis, voluptatem distinctio unde! Exercitationem beatae, provident sapiente suscipit natus magnam animi accusamus dolorem?</section>
        </article>
        <article>
            <h2>article 3</h2>
            <section>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium iure tempore fugit quibusdam corporis maiores tenetur aut, inventore sunt cumque veritatis pariatur, ea laboriosam velit esse mollitia ratione necessitatibus facere.</section>
            <section>Ipsam vitae omnis dignissimos eos officiis tempora exercitationem adipisci nulla quisquam impedit incidunt delectus quibusdam eius illum voluptates ea, nam voluptatum animi quis ut iure! Sit laudantium nesciunt minima quaerat.</section>
            <section>Quaerat ab pariatur autem accusamus quam eligendi, reprehenderit recusandae vitae minus reiciendis consequuntur magnam minima atque, repellendus doloribus optio nisi beatae, consectetur alias dolorem debitis natus accusantium corporis at! Veritatis!</section>
        </article>
    </main>
</body>
</html>
```

## JSON

JSON stands for _JavaScript Object Notation_. It's a standard way of encoding objects as strings. It can be, and often is, used across many languages. Objects can have properties, are represented as keys, which are always strings, and values, which can be strings, numbers, arrays, or objects. The following is an easy-to-understand example:

```JSON
{
    "name": "bob",
    "credit": 12,
    "children": [
        {
            "name": "mary"
        },
        {
            "name": "Reginald"
        }
    ]
}
```

Note whitespace is ignored, double quotation marks are mandatory, and extra commas are syntax errors.   

## <a name="markdown"></a> MarkDown

MarkDown is a simple markup syntax that allows very fast document composition with little effort, and leaves the files fairly readable. It is very simple to learn, and can be illustrated very quickly in a few short examples. It focuses on headers, tables, links, images, emphasis, and division of pages. It's often interspersed with HTML. Below is the markdown that created this definition:

```markdown
## <a name="markdown"></a> MarkDown

MarkDown (abbreviated 'md') is a simple markup [syntax](#syntax) that allows very fast document composition with little effort, and leaves the files fairly readable. It is very simple to learn, and can be illustrated very quickly in a few short examples. It focuses on headers, tables, links, images, emphasis, and quotes. It's often interspersed with [HTML](#html). Below is the markdown that created this definition:
```

[This page](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is very short, but explains 95% of everything you'll ever need to know with markdown. You can also look at the entire source of this documentation site in markdown [here](https://gitlab.oit.duke.edu/colab/external-docs/tree/master).

## <a name="nodejs"></a><a name="node.js"></a><a name="node"></a>Node.js

Node.js is a runtime for Javascript. It's a program that will run Javascript code in the console for you, as opposed to in a web browser. It has gained a good deal of popularity because it allows people to write web servers in the same language that they wrote their front-end (browser) code in. It comes bundled with npm, and has also gained a lot of popularity because of the many, many packages available.

## <a name="npm"></a>npm

npm is the Node.js package manager. It allows you to easily add external modules to a nodejs project. It also is frequently used to install simple applications, since it is cross-platform and works very well with [git](#git) repositories. It's installed whenever you install node. You can browse all of the available packages [here](https://www.npmjs.com/), and see an example of usage below. 

```bash
#npm in the terminal

npm install -g gitbook  #installs gitbook globally, allowing us to use the next command
npm install express #downloads the 'express' module (and dependencies) and puts it in this project's node_modules folder

```

```javascript
//using modules installed by npm in a node.js program
var express = require('express');//modules in the node_modules folder in your project are accessible just by their name


```

## <a name="rsa-keys"></a> RSA keys

Also called public/private keys, keypairs, or some variant, rsa keys are a very powerful way of encrypting and signing messages. With RSA, you have two keys. One is public, which you share with anyone who asks. The other is private (also called secret), and you keep it just to yourself. If someone wants to send you a message only you can read, they sign it with your public key. Math makes it so that no one can decrypt it with anything but your secret key. If you want to write a message and sign that it is authentic, you can encrypt it with your private key. Then, only your public key can decrypt it. Since everyone knows it, they can all verify that you, in fact, did write your message. 

## <a name="runtime-error"></a> Runtime Error

A runtime error is exactly what it sounds like: an error that occurs while your program is running. It'll often happen when variables aren't initialized properly, or your program hits an unexpected condition. They're typically harder to debug than compilation-time errors like syntax errors, because those can be caught before a program even compiles.

## <a name="server"></a> Server

'Server' is a funny word, because it can take a few meanings. When you're talking about software, it typically means a long-running program that provides a service, like sending a web page back when one is requested. At the same time, you'll often hear the machines that are used to run such programs referred to as servers as well. These machines are typically configured differently from your average desktop or laptop, usually having no monitor or keyboard (only allowing ssh connections to modify them), and often sporting error-correcting RAM and multi-port network cards.

## <a name="ssh"></a> SSH

SSH stands for 'Secure Shell'. It's a very powerful way to remotely access other machines (also called hosts) from your terminal. the commands follow the format `ssh username@host`, where 'host' can be a hostname like example.com or an IP address like `127.0.0.1`. After running that command, you'll find yourself typically asked for the password for that user on that machine. If you supply it with the -i flag, as in `ssh toolbox@colab.duke.edu -i /ssl/private/myKey.key`, you can use a secret key file instead of a password to authenticate, provided you have set it up on the remote machine.  

## <a name="sudo"></a> sudo

sudo is like ['Simon Says'](https://xkcd.com/149/) for unix (mac and linux) machines. When you prefix a command with sudo, you're essentially saying 'run the following command with root permissions'. 

```
toolbox@server:~$ touch /thebutt
touch: cannot touch ‘/thebutt’: Permission denied
toolbox@server:~$ sudo touch /thebutt
toolbox@server:~$ #Successful finding nemo joke
```

Careful though, because ant files you creeat with sudo, will be, by default, owned by root, and will require sudo to edit them in the future. If you want to create a file and need sudo, but want to be able to edit it without sudo later, use `chown myuser filename`.

## <a name="syntax"></a> Syntax

Syntax (roughly) is the grammar of a programming language. If you put things in the wrong order, or mess up their structure, it's usually a syntax error. Forgetting a paretheses is usually one, as would usually be mashing keys and trying to run it as a program. Syntax errors are the easiest type of error to fix.

## <a name="yaml"></a> YAML

YAML is intended to be an even more human-readable version of JSON. it is a strict superset of JSON, meaning all JSON is valid YAML, but not necessarily the other way around. Instead of explicitly showing parent-child relationships with curly braces, it allows whitespace to be used instead. The [YAML wikipedia page](https://en.wikipedia.org/wiki/YAML#Language_elements) should give much more detail.

This is some YAML I stole from wikipedia:
```yaml
receipt:     Oz-Ware Purchase Invoice
date:        2012-08-06
customer:
    first_name:   Dorothy
    family_name:  Gale

items:
    - part_no:   A4786
      descrip:   Water Bucket (Filled)
      price:     1.47
      quantity:  4

    - part_no:   E1628
      descrip:   High Heeled "Ruby" Slippers
      size:      8
      price:     133.7
      quantity:  1
```