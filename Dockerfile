FROM stevedore-repo.oit.duke.edu/el7-nginx:latest
LABEL maintainer="Victor Wang <xw72@duke.edu>"

ADD tests.d/* /tests.d/
ADD ./dist /var/www/html
