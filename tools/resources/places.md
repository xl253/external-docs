# What is this about?

The goal of any "Places" API is to give applications metadata (or data describing your data) to add more meaning to locations. The places API lists a great number of the places at duke, whether it's the DukeCard office, or the Link, or ABP. Each of these places has a series of tags and other metadata, including opening/closing times.

## /places/categories

This directory of the places API describes the various categories with which a "Place" can be described by the API. It returns an array of objects of the format below. Category is the name of the category. Ignore url and thumbnail_url, as they are unused. cat_id is a percent-encoded group of tags that lists the category and its parents, for using in later API calls. no_child_cats is false if the category has children, and children contains these children (who have the same format) in an array.

```javascript
{
        "category": "Location",
        "url": "https://www.dukeonline.duke.edu/places/items/show/?tag=dining",
        "cat_id": "dining",
        "thumbnail_url": "",
        "no_child_cats": false,
        "children": []
}
```

## /places/items

This directory holds an actual listing of places, with an optional tag parameter. With the tag (whose valid values are any of the categories), the API returns an array of place objects whose tags contain the tag specified. If tag is omitted, the API returns a list of all places. The place object is show below. Each place has an arbitrary and unique place_id to make it easy to search through lists of them and make further API calls. The name is the location's written name, and is not recommended for use as an index (use place_id's instead!). The location describes the building that contains the place. position is fairly straightforward. The tags array contains a list of all tags (categories) that describe the place. phone is the place's phone number as a string, if applicable, and open is a string, "true" or "false", describing the open/closed state of the place.

```javascript
{
    "place_id": "20096",
    "name": "DukeCard Office - Medical Center",
    "location": "Facilities Center",
    "position": {
      "latitude": "36.004363",
      "longitude": "-78.935995",
      "google_map": "http://maps.google.com/maps?ll=36.004363,-78.935995"
    },
    "tags": [
      "dukecard",
      "service_desk",
      "west_campus"
    ],
    "phone": "919-684-2273",
    "open": "true"
  }
```

## /places/items/index

This call takes a mandatory parameter place_id, and will throw an error without it. It returns the place object associated with the place_id that was input, with two new fields. The first field, schedule, contains an array called open_close which itself contains objects describing the dates and times of the next week's openings and closings. The other field, extensions, is an array of objects that each have a label and a value. These are not highly regulated and just describe extra information about the place.

# How do I use it?

As with all of the duke APIs, all you have to do is send an HTTP GET request to (aka pull down the webpage of) the url listed, appending "?access_token=YOURTOKEN", as well as "&PARAMETER=VALUE" as necessary. For this particular API, it is highly recommended to pull the data down only once a day or so, and cache this data on your user's device. This saves time and bandwidth. The places API data doesn't change much, so there's no need to keep polling it. That will only slow down your application.