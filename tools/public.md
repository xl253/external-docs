# Duke Public APIs

Duke has made several APIs available for development purposes. These are used by developers to get data about various aspects of Duke.

API stands for Application Programming Interface. It's a scary acronym, but all they do is describe a way of allowing your programs and apps to get information from (or send data to) someone else's program. For example, if I wanted my app to be able to access the Duke Directory for a user, I'd use the Directory API to get the information. To learn more about APIs in general, visit the wiki page. To learn the best ways of interfacing, and to get more of a how-to approach, read the documentation here, and consult with our tutorials and sample code.

OIT has created a service called streamer to gather up all of their public APIs, so you only have to go to one place to access them. Streamer responds to requests for pages (via [HTTP](http://rve.org.uk/dumprequest)). In order to get data from streamer, you must request a page, like streamer.oit.duke.edu/ePrint/departments. The response pages contain a single [JSON](http://www.copterlabs.com/blog/json-what-it-is-how-it-works-how-to-use-it/) object. Most languages have simple libraries that can do HTTP requests and manipulate JSON fairly easily. If you're not familiar with JSON, or how to make requests for pages via HTTP, I suggest looking both topics up on Wikipedia. The articles are quite informative.

These APIs encompass a large part of the information available, but if you have something that you really want to get your hands on, just give us an email! We'll see if we can build an API so that everyone can benefit.

| API                                                             | Description                                                                                                                                        |
|-----------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------|
| [Curriculum](resources/curriculum.md) | Allows you to get a list of all classes, subjects, synopses, you name it!                                                                          |
| [Directory](resources/directory.md)   | The directory API allows you to search for publicly available information about people and netids                                                  |
| [Places](resources/places.md)         | The Places API provides a method of looking through all of the "Places" on campus. Food venues, lecture halls, etc. are indexed in the Places API. |
| [Social](resources/social.md)         | The Social API provides a readout of all of the social media postings under Duke official accounts.                                                |
| [Eprint](resources/eprint.md)         | The ePrint API gives locations, statuses, and other information about the printers that use the ePrint system.                                     |

The javascript snippet below demonstrates code for accessing one of the streamer APIs. In order to get data, you need to allocate yourself an access token from streamer.oit.duke.edu.
You pass the query parameters specified in the documentation, as well as the API key, in as GET variables. For example, to search the directory for a netid of 'jpo3', you would request the page:

[https://streamer.oit.duke.edu/ldap/people?q=jpo2&access_token=XXXXXXXX](https://streamer.oit.duke.edu/ldap/people?q=jpo2&access_token=XXXXXXXX)

```javascript
var access_token = "XXXXXXXXXXXXXXXXXXXXXXXX";
var query = "jpo3";

function response() {
    console.log(this);
    var data = JSON.parse(this.responseText);
    alert(data.netid);
}

request = new XMLHttpRequest();

request.onload = response;
request.open("GET","https://streamer.oit.duke.edu/ldap/people?q="+query+"&access_token="+access_token,true);
request.send();
```