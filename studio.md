# Co-Lab Studio

Check out these guides to learn how to use the machines in the Studio.

* [Lockers in the studio](studio/locker.md)
* [Print to the laser cutter with Adobe Illustrator](studio/printing-to-laser.md)
* [Up and Running with Open GigaBot](studio/open-gigabot.md)
* [Fusion Export to DXF](studio/fusion-dxf.md)
* [SketchUp to STL](studio/sketchup-stl.md)
* [SketchUp to DXF](studio/sketchup-dxf.md)
* [Epilog Fusion](studio/epilog-fusion.md)

## Premium services
* [3D printer service (paid version)](3d-printing-service.md)
