# Up and Running with Open GigaBot
This tutorial will run through the operation of the Open GigaBot from STL to finished 3D-printed piece. Note that you need simplify3D to do the slicing.

## Terminology
- Console: the screen that is mounted to the right of the Open GigaBot Gantry. The console screen can swivel and behind it is a Raspberry Pi 3.
- Drive Gear Tensioner: the black L-shaped covers of both the left and right extruder assemblies. This cover serves to keep the filament in tension so that it can be directly driven by the stepper motors. Each is composed of the cover, two M3 screws, and two springs.
- Filament Rack: the modified assembly sitting atop the gantry, consisting of a 2x4 piece of wood, 3D printed bearing mounts, and two rolling shafts.
- Filament Routing: the white teflon tubes flanking the interior corners of the gantry. There are two total filament routing paths: one on the right, and one on the left.
- Gantry: the main body of the Open GigaBot. Dimensions are approximately 2'x2'x6'. The 8020 aluminum extrusion body is paned by thin polycarbonate windows and features a modified filament rack on top of it.
- Toolhead: the moving part of the gantry that holds both extruders.

## Hardware Check
1. Verify that both the OGB Gantry and the mounted Raspberry Pi 3 are plugged into the outlet.
2. Verify that the USB cable from the OGB Gantry is plugged into the Raspberry Pi 3.
3.  Turn on the OGB Gantry by flipping the On/Off switch on the upper left side of the metal frame. The fan behind the double polycarbonate doors should should whirr.
4. Verify that each filament routing path has a piece of filament inserted such that the endstops are engaged.
5. Verify, with calipers, that the length of both compression springs in front of the drive gear tensioner are 14.5 mm.

## Software Check
1.  Plug in a keyboard to the console.
2. Verify that the command line is available.
3. Verify that the 'Open GigaBot' is available on 3DprinterOS.

## Converting from STL and Slicing
1. Download the latest FFF profile [profile]


----------


(https://www.dropbox.com/s/xxg36kwyn2uy9vw/OGB_Release.fff?dl=0) from Dropbox.
2. Open simplify3D and with File>Import FFF Model, choose 'OGB_Release'.
3. Load your STL file by dropping it onto the bedplate preview.
4. Select 'Prepare to Print' on the left panel of the simplify3D interface.
5. Select 'Save Toolpaths to Disk' to generate a .gcode file.
6. Upload the gcode file to 3D printerOS.
7. Select "PRINT" and choose 'Open GigaBot' from the list of printers.
