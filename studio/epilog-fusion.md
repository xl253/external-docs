### Epilog Fusion 60W Materials Settings
This document lists settings for common materials for the large laser cutter.

|      Material     | Thickness | Raster (Speed/Power) |  Vector (Speed/Power/Frequency)  |
|:-----------------:|:---------:|:--------------------:|:--------------------------------:|
|      Acrylic      |    1/8"   |        90s 45p       |           8s 100p 100f           |
|                   |    1/4"   |        90s 45p       |           2s 100p 100f           |
|                   |    3/8"   |        90s 45p       | Use 1/8" settings multiple times |
|                   |    1/2"   |        90s 45p       | Use 1/8" settings multiple times |
| Anodized Aluminum |           |        90s 35p       |            Do not cut            |
|       Glass       |           |       30s 100p       |            Do not cut            |
|     Wood/ MDF     |    1/8"   |       60s 100p       |           10s 100p 10f           |
|                   |    1/4"   |       60s 100p       |            3s 100p 10f           |
|                   |    3/8"   |       60s 100p       | Use 1/8" settings multiple times |
|                   |    1/2"   |       60s 100p       | Use 1/8" settings multiple times |
|                   |           |                      |                                  |
Optimizing laser cuts is an iterative process. Some basic guidelines for adjusting settings to optimize outcomes are:

- If the engraving is too dark, *increase* speed or *decrease* power.
- If the darkest shade on an engraving is too dark, lower the power by 10% first. Power setting controls the upper limit of darkness of the engraving.
- In all other cases, modulating the speed of the laser offers finer control of shading. Modulate in increments of 5%.
- For cutting very thick materials, use incremental cuts at settings for thinner materials to get through it more cleanly. Cutting through thick materials has greater risk of burning.
