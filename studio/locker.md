# Locker Documentation

## How to Lock a locker

1. Press “C” button
2. Type a 4-digit code
3. Press lock icon button

## How to unlock a locker

1. Press “C” button
2. Type your 4-digit code
3. Press lock icon button