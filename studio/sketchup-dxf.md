# SketchUp to DXF

If you have SketchUp Pro:

When you export a SketchUp model as a 3D CAD file, you can select what entities are exported. To export your model, follow these steps:

1.  In SketchUp Pro, select File > Export > 3D Model. The Export Model dialog box appears.
2.  Navigate to the location where you want to save your exported file.
3.  (Optional) Change the file name if you like. By default, the exported file uses the same name as your SketchUp file name.
4.  Select either `.dwg` or `.dxf` as the file format for your exported file. In Microsoft Windows, select your file type from the Export Type drop-down list. In Mac OS X, use the Format drop-down list.
5.  Click the Options button to open the Export Options dialog box, shown in the following figure. From the AutoCAD Version drop-down list, select the version of AutoCAD you’d like to use to open the exported file. In the Export area, select the checkbox for each type of entity that you want to include in the exported file. Click OK when you’re done.
6.  Back in the Export Model dialog box, click Export, and your file appears in the location where you chose to save it.

If you are using the free SketchUp software, do the following.

1.  Go to [http://sketchupplugins.com/plugins/convert-sketchup-skp-files-to-dxf-or-stl/](http://sketchupplugins.com/plugins/convert-sketchup-skp-files-to-dxf-or-stl/)
2.  Download the plugin, follow the online instructions to install.
3.  In SketchUp, go to Tools->Export to DXF or STL
4.  Select your units.
5.  Select “polyface mesh”
6.  Save.
