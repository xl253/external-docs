# Fusion Export to DXF
This tutorial seeks to delineate the process of exporting a sketch in Fusion 360 to an explicit DXF file.

1. Navigate to the feature tree, which will begin with "Unsaved" or your file name.
2. Navigate down the tree to "Sketches".
3. Right click the sketch you want to export to DXF.
4. Select "Save as DXF".
5. Choose a file name and local computer destination.
