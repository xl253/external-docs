# SketchUp to STL

So you designed something in SketchUp and you are ready to 3D Print it at the Innovation Studio, but wait!!! When you try to export it, there may not be an option for an STL file. Follow these steps, and you’ll be picking up your print in no time.

1.  Open up SketchUp and navigate to the “Window” tab.
2.  Go down and select “Extension Warehouse”
3.  Inside the warehouse, search SketchUp STL
4.  Install the extension
5.  Back in the file with whatever it is you want to 3D print, click File->Export STL
6.  Select your units.
7.  Click Export
8.  Celebrate!
