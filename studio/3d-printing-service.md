# CoLab 3D Printing Service

The CoLab obviously has free printing available for users, but for those who want more detail and a wider range of functionality we have many other printers that offer a variety of levels of precision and types of materials. These other printers function on a pay-per-use basis and in order to use the service you need the following things:

1. Your part will need to be in .stl file format
2. Either a credit card or a Fund Code with which to pay

The directions for using the service are as follows:

1. Look at print types and pricing: navigate to https://3dprint.duke.edu/3d-printing-service. Find one that best suits your needs or if you still have questions email us at colab-ops@duke.edu and we will connect you with one of our student workers who can best help you narrow down the options
2. Submit your file: navigate to https://eprint.oit.duke.edu/3dprint/ and click ‘Request an Estimate’ at the bottom of the page. Do not worry about properly filling out the drop-down list – simply attach your .stl file and write in the comments section which type of printer you want and the quantity of the part desired.
3. Approve the quote: we will get back to you with a quote within 48 hours and you will receive an email from colab@duke.edu with the title ‘JOBXXXX Status Update’. At the bottom of the email will be a link which you should follow that will take you back to the eprint site. There you must change the ‘STATUS’ of the Job from ‘Quoted’ to ‘Approved’. This notifies us to start the print
4. Pick up the print: we will let you know once your part is completed and ready for pick up. Turnaround time is less than 3 business days
5. Pay for the print: after the part finished printing we will send you an invoice for the part where you can enter your payment information. Cards are charged at the end of each month

Thanks for using our service!
