# Guides

The guides are here to help you get a feel for anything you might want to be able to do. It's hard to hear a textbook description of a web server, and then go out and build one. It makes much more sense if you see someone else do it first.

Building, maintaining, designing, and understanding software is an important skill, often divorced from the more elegant theories of computer science. This is the good, bad, and ugly stuff that'll help you get whatever you want to build up and running!

 - [Continuous Integration](guides/ci.md): This example will show you how you can automate the process of building, testing, and deploying your application, using this site as an example!
 - [Using Web APIs](guides/apidocs.md): This example shows you that the term 'API' doesn't have to be scary, walking you through using the Duke API in JavaScript.
 - [Getting a server from Duke](guides/vm-manage.md): Duke provides a lot of development resources for you to learn with, including your own personal server. Learn how to set it up!
 - [Using ssh to control your servers](guides/ssh.md): ssh is the standard way of logging into servers and other remote machines, and running stuff on them.
