pipeline {
  agent {
    node {
      label 'atomic'
    }
  }
  stages {
    stage ('Build Content Image') {
      steps {
        sh '''
          source /build/pipeline_vars
          echo "Building colab/external-docs:build"
          docker build -t colab/external-docs:build -f Dockerfile.build .
        '''
      }
    }
    stage ('Extract Source files') {
      steps {
        sh '''
          source /build/pipeline_vars
          echo "Extracting build product"
          docker create --name $BUILD_TAG-$BRANCH_NAME-extract colab/external-docs:build
          docker cp $BUILD_TAG-$BRANCH_NAME-extract:/app/_book ./dist
          docker rm -f $BUILD_TAG-$BRANCH_NAME-extract
        '''
      }
    }
    stage ('Build Server Image') {
      steps {
        sh '''
          source /build/pipeline_vars
          build
        '''
      }
    }
    stage ('Cleanup non-standard build artifacts') {
      steps {
        sh '''
          source /build/pipeline_vars
          docker rmi colab/external-docs:build
          rm -rf ./dist
        '''
      }
    }
    stage('Test') {
      steps {
        parallel (
          Integrity: {
          sh '''
             source /build/pipeline_vars
             test
          '''
         },
         SysSecurity: {
             echo "Placeholder"
         }
       )
      }
    }
    stage('Deploy') {
      when {
        branch 'master'
      }
      steps {
        sh '''
           source /build/pipeline_vars
           tag
           '''
      }
    }
  }
  post {
      always {
          sh '''
             source /build/pipeline_vars
             cleanup_images
             '''
      }
      failure {
        mail(from: "doozer@build.docker.oit.duke.edu",
             to: "victor.xw@duke.edu",
             subject: "External-docs Build Pipeline Failed",
             body: "Something failed with the Colab External image build: https://build.docker.oit.duke.edu/blue/organizations/jenkins/colab%2Fexternal-docs/activity")
      }
  }
}
