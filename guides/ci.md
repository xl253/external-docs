# How to Use Gitlab CI to Auto-Build this Site

This guide requires you to know:
 
 - [How to install Node.js](languages/javascript/node-installation.md)
 - [How to use Bash (the terminal)](languages/bash/intro.md)
 - [How to use Git](git.md)
 - [How to Allocate a Server](vm-manage.md)

---------------

Whenever somebody pushes a change to the git repository for this site, [CI](../glossary.md#ci) automatically builds it (double checking that it actually can be built), and then deploys it to the public site, all in just a minute or two. This is done without anyone telling it to do so! Let's see how...

## 0: Before starting...

Before you start, you'll need:
 - Your laptop or desktop
  - with Node.js installed 
  - `npm install -g gitbook` to install gitbook on it
 - A VM-Manage server running Ubuntu 14.04 or Debian Jessie
  - with Node.js installed 
  - with Docker installed ([How-To](https://docs.docker.com/engine/installation/))
  - with Gitlab-CI-Multi-Runner installed ([How-To](https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/linux-repository.md))
  - Run `sudo apt-get install apache2` to install apache on the server

## 1: Set up your repository

This part is really simple! Just create a [new project](https://gitlab.oit.duke.edu/projects/new) in Gitlab use `gitbook init` to add the first [markdown](../glossary.md#markdown) files to it.

![step 1](ci/step1.gif)

We're just using `Gitbook` to build our site (turn our markdown into html). You can use whatever you'd like to build your own projects, or make projects that don't require building at all!

## 2: Set up `.gitlab-ci.yml`

Gitlab CI uses a [YAML](../glossary.md#yaml) file to describe what you'd like to have done when building, testing, and deploying your project. We're going to use the file below for this project:

```yaml
image: node:6

build:
    stage: build
    artifacts:
        paths:
         - _book
    script: 
     - npm install gitbook-cli && node node_modules/gitbook-cli/bin/gitbook.js build
    tags:
     - build-capable

test:
    stage: test
    script: 
     - echo "no tests."
    tags:
     - test-capable

deploy:
    stage: deploy
    script:
     - rm -rf /var/www/html/*
     - cd _book
     - cp -rf . /var/www/html
     - echo "this is where it'll distribute itself wherever is necessary!"
    tags:
     - deploy-capable
    only: 
     - master

```

Add this file to your project and commit it.

Some useful information about the fields in this file:
- `build/test/deploy`: These are the stages supported by the GitLab CI and are execute in the order of `build` -> `test` -> `deploy`. The CI will continue to the next stage only if the previous stage succeeded without error.
- `script`: This field is central to GitLab CI because the tasks CI will run is defined by bash scripts. This is the place to put your bash scripts. If you put multiple lines, execution will only continue if the previous line succeeds.
- `artifacts`: This field specifies the artifact for the current stage. Artifact is result of execution and will be passed as a whole to the next stage(s).
- `tags`: You have the option to specify tags so that a specific task will only run if the runner's tag matches the tags specified here. Runner's tag is created when setting up the runner. For more information about setting up runners, please see https://gitlab.com/gitlab-org/gitlab-ci-multi-runner
- `only`: This tag let you specify the condition on which the task is executed. Here `only: master` will make the CI run the deploy task only when there's a commit pushed to the `master` branch.

![step 2](ci/step2.gif)

You'll notice that next to your commit in gitlab, you'll see a little icon like this, indicating that CI is enabled for your build, but you don't have any runners yet: 

![pending icon](ci/pending.png)

## 3: Creating Runners

Runners are the things that actually execute the Gitlab CI config. We're going to configure two different ones on the server you set up.

Go to your project on Gitlab, click settings, and click 'runners'. We're going to use the CI URL they have show on the left, as well as the registration token.

Now we're going to create two runners on your server. One will do building and testing, while the other will deploy if the build and test stages complete! Run:  

```bash
sudo  gitlab-ci-multi-runner register
```

Give it a name, and the tags 'build-capable' and 'test-capable'. These tags, combined with our configuration, make it so that this runner will do both building and testing. Pick 'docker' as the executor, which keeps all the building and testing in a docker image. Pick the default image 'node:6'.

![Registering the first runner](ci/step3.1.gif)

You might notice the icon next to your commit in Gitlab switch from 'pending' to 'running'. The build and test steps have begun! Now we just need to enable the deploy runner.

By default, Apache serves up the path `/var/www/html`. We need to clear it out and give Gitlab permissions to edit it, so run this:

```bash
sudo rm /var/www/html/*
sudo chown root:gitlab-runner /var/www/html
sudo chmod 775 /var/www/html
sudo gitlab-ci-multi-runner register
```

This time we register it with the deploy-capable tag, and using the 'shell' executor. This runs the script in the deploy stage of our .gitlab-ci.yml file to be run straight on the server, every time build and test are successful.

![registering the second runner](ci/step3.2.gif)

If all went well, you should see the green 'passed' icon next to your commit in gitlab, and you'll immediately be able to go to `http://colab-sbx-XXX.oit.duke.edu` and see your published site!

## Further Thoughts 

There are so many, many things that can be done with CI. This setup builds a static html site out of some markdown files, but you can configure it to compile your big webserver, run all your unit tests, and send it to your server from somewhere else. Your runners can be on any machine you want (even your laptop!). Definitely explore the many different ways you can use CI. For more reference you can use the [Gitlab CI Reference Page](../reference/gitlab-ci.md).

## Participants of the Git to Deploy /roots Class
- Abraham Ng'hwani
- Mick Benham
- Michael Raguso
- Mike Faas
