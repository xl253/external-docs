# SwiftyDuke

A Swift 4 library designed to connect to the API's maintained by the Innovation Co-lab at Duke University. 

## The APIs

#### Social Media 
The SDSocial wrapper class provides access points to the social media API. You can use the SDSocial.shared() singleton to access the data provided. We provide 3 different ways to get this data: all of the data at once, filtered by a search term, and filtered by the media source (Twitter, Facebook). 

```
SDSocial.shared.getSocial(accessToken: SDConstants.Values.testToken, error: { (message) in
	alertController.removeLoadingIndicator()
	alertController.title = "Authentication error"
	alertController.message = message
	alertController.addDismissalButton()
	}, completion: { (result) in
		self.dismiss(animated: false, completion: nil)
		self.posts = result
	})
```
Is one example of how you may use the social media API. You can pass in the error handling function as well as the completion function.

#### Identity 
The SDIdentityManager wrapper class provides access points to the identity APIs. With these, you can search for a person affiliated with Duke by their unique netID, uniqueID, or name. These functions look at massive amounts of data, so filtering them out tends to work best. 
```
SDIdentityManager.shared.personForNetID(netID: netid, 
accessToken: SDConstants.Values.testToken, 
error: { (message) in
	self.handleDataError(message: message)
	}, completion:  { (personInfo) in
		self.selectedIdentity = personInfo
		DispatchQueue.main.async {
			self.performSegue(withIdentifier: "identitySegue", sender: self)
		}
	})
```
You can pass in any type of error handling and completion handling function you wish.

#### Curriculum 
The SDCurriculum wrapper class provides access points to to curriculum that Duke offers. To use this API, you may pass in subject terms and get courses back, or you may pass in course IDs and get information about that course back. 
```
SDCurriculum.shared.getCoursesForSubject(subject: self.selectedField, 
accessToken: SDConstants.Values.testToken, 
error: self.handleDataError) { (classes) in
	self.courses = classes
	DispatchQueue.main.async {
		self.tableView.reloadData()
	}
}
```

#### Places 
The SDPlaceManager wrapper class provides access to the places API. This gives information about places on Duke's campus. You can use it in several different ways: simply getting the place categories, getting a place for a specific tag, or getting a place for a specific ID. 

#### Authentication 
You may authenticate a user on the Duke network by using the SDAuthenticate wrapper class. The method requires a clientID and a redirectURI. 

