# Discover

An Innovation Colab Project to match ideas and developers in order to bring more creative ideas to life.

## Projects

After logging in with Duke oAuth, you will be able to see all projects that have been posted. If you are a developer with a specific skill set, you may filter by any one of the skills or interests on the left hand side of the page.

#### Creating a project
If you are someone with a project idea, you may create a new project by clicking the "Create A Project" button. Here, you can add a project name, as well as any specific skills or interests the project may entail. It is useful for developers to know a bit about what the project is about, so try to put as much as you feel comfortable about the project in the description section.

#### Matching a project


## People
The people tab allows the user to see all the current developers. You may click on a user to learn more information about them and contact them.

## Your Profile
You may additionally view and edit the information that others see about you by clicking on the "Profile" tab. You may change your specific skills and interests, upload a profile picture, and let others know what you are studying. 
