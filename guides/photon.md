##Set Up your Photon
---------------

### Step 1: Install Particle CLI:
#### If you are a Windows user:
You need to download [Particle Windows Installer](https://binaries.particle.io/cli/installer/windows/ParticleCLISetup.exe). This installer can install everything you need automatically.
#### If you are a Mac/Linux user:
You can install particle-cli by copying `$ bash <( curl -sL https://particle.io/install-cli )` to your terminal, and it will be automatically installed.

Or you can use npm for manual installation:

First, you need to setup Node.js from [here](https://nodejs.org/en/).
We highly recommend you using [homebrew](https://brew.sh) for the installation.
Open your terminal, 
````bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew update
brew install node
````
After the installation, type `$ npm -v` in your terminal to see if you have it property setup.
Then type `$ npm install -g particle-cli`.
This command will install Particle CLI to your computer.
#### If you are an iOS/Android user:
(This method is highly unstable and might not work on some devices)
Connect your Photon to the charger (or any USB ports on your computers).
Download the Particle App from APP Store or Google Play Store: [iOS](https://itunes.apple.com/us/app/particle-build-photon-electron/id991459054?ls=1&mt=8) | [Android](https://play.google.com/store/apps/details?id=io.particle.android.app).
Follow the in-app directions to connect and setup your Photon.
After opening the app, press "Get Started", to arrive at the device list screen. Scroll to the bottom and select Setup a Photon.
You have to open your settings and connect to Photon's Wi-Fi to connect to your device.
The App will automatically set your Photon up after you return to the app.


### Step 2: Set up Your Particle Account.
If you haven't created a Particle account, [click here](https://login.particle.io/console) to create a new account. After you have an account, open your terminal/command line and type `$ particle login`, then follow the instructions to log in to your Particle account.

### Step 3: Connect your photon using the USB
Connect your particle device to your computer.

### Step 4: Get the Mac Address of Your Photon (not required)
Proceed to Step 6 if you do not have a netID or prefer to setup later
In your terminal, run `$ particle list` to see if your Photon is properly connected. 
Then run `$ particle serial mac` to get the photon’s mac address (must be in green listening mode)

### Step 5: Register Your Device to Duke Open (not required)
Register your photon at https://dukereg.duke.edu/. (You will need to login with your NetId).
Using the mac address you retrieved from step 4, choose a name, and select other for OS options.

### Step 6: Claim and Set up Your Photon
In your terminal, run `$ particle setup` and then CAREFULLY follow the instructions on the screen. You may need to press the SETUP botton of your Photon at the beginning so your computer can recognize it. When selecting Wi-Fi options, if you followed step 4 and 5, be sure to use DukeOpen and choose none for security settings. Otherwise, please choose DukeVisitor or your own mobile hotspot and choose none for security settings. (You may manually type these information or let particle to scan the SSID and security options for you)
You should be good to go after it finishes that. Your Photon will automatically reboot after finishing those settings. You’ll know it’s connected when it is breathing blue.

!Important: If you received error when using `particle setup`(often occurs when you failed the Wi-Fi settings at the first attempt and start again), this means you've already claimed your device. Please close your terminal and open a new window or end the session with "control + c". And use the `$ particle serial wifi` function to set up the Wi-Fi.
Please note: If you are in a place where more than one Photon is being set up or if you’re experiencing errors with the `particle setup` command, please refer to steps 4-6 of the following guide to set up your device: https://gitlab.oit.duke.edu/colab/IoT-Tutorials/blob/master/PhotonSetupGuide/PhotonSetupGuide.md


### Step 7: Dive Into Projects
Visit https://gitlab.oit.duke.edu/colab/IoT-Tutorials and https://learn.sparkfun.com/tutorials/sparkfun-inventors-kit-for-photon-experiment-guide to learn more about your Photon, view IoT project ideas, and try some of your own!    

Got questions? Need help on a project? Visit us at [office hours](https://colab.duke.edu/resources), [Studio Nights](https://colab.duke.edu/studionight), ask a question in our [Slack group](dukecolab.slack.com), or come to an IoT [Roots course](https://colab.duke.edu/roots) at the Co-Lab. 

### References and External Links
[Official Documentation](https://docs.particle.io/guide/getting-started/intro/photon/)
[Online IDE](https://build.particle.io/)
[Photon RedBoard Hookup Guide](https://learn.sparkfun.com/tutorials/photon-redboard-hookup-guide)
[SparkFun Inventor's Kit for Photon Experiment Guide](https://learn.sparkfun.com/tutorials/sparkfun-inventors-kit-for-photon-experiment-guide)
[Serial Communication](https://learn.sparkfun.com/tutorials/serial-communication)
