# Duke OAuth Login Guide

## Useful Documentations/Links

[CoLab App Manager](https://appmanager.colab.duke.edu/)

[OAuth Bible](http://oauthbible.com/)

[Documentation from CoLab](https://documentation.colab.duke.edu/guides/apidocs.html)

[CoLab APIs](https://apidocs.colab.duke.edu/)

[RFC 6749 for geeks with no life and a shit load of time](https://tools.ietf.org/html/rfc6749)

## What to do

You want to use the Two Legged 'Implicit Grant' flavor of OAuth 2. If you happen to pick the three legged version by any chance, you would get stuck on what's called a CORS error for hours or even days without a valid solution. God bless you.

The workflow:
1. Register a new app on [CoLab App Manager](https://appmanager.colab.duke.edu/), for permission level you can just choose basic. For the redirect_uri though, remember to put in addresses with the `http://` or `https://`, otherwise you won't pass the check.
If you're testing on `localhost`, simply add for example `http://localhost:1717` as another redirect_uri. Yes you need the port number. When you get to production you then want to add your productioin url.

2. In your code, when the user clicks the log in button, make a redirect to `https://oauth.oit.duke.edu/oauth/authorize.php`. Not done yet! Encode these parameters IN YOUR URL:

	- response_type = 'token'
	- redirect_uri: The OAuth bible says this is optional, but you need this because you probably have more than 1 uri registerd in AppManager. Make sure that your uri here matches with what you put in AppManager. For example when you're testing do the localhost one.
	- scope: You just need 'basic' for login and simple api requests.
	- state: Optional, this is for security bluh that you won't need, you can put some random number here if you want.
	- client_id: the client_id that you registerd on AppManager.

	So when I did this with our group's app, the url that we're redirecting to looks like this:

	`https://oauth.oit.duke.edu/oauth/authorize.php?client_id=kungfoods&client_secret=imnotputtingoursecretheredoyouthinkimdumb&redirect_uri=http%3A%2F%2Flocalhost%3A1717&response_type=token&state=1129&scope=basic`

	And then your use will see the Duke NetID login page. After log in Duke OAuth service is going to redirect back to your redirect_uri.

3. Continue on. The redirect_uri is going to come back to you with access_token encoded in the url. Yes we're there already. When we did this the url we got back looked like this:

	`http://localhost:1717/#access_token=imnotgivingyoumytokendoyouthinkimdumb&expires_in=3600&token_type=Bearer&scope=basic&state=1129`

	Yeah it comes in as a hash. For JavaScript you can get the hash part with `window.location.hash`. Then you take the `substring(1)` and with NodeJS' `querystring` package's `parse` function you can get the query params as an object. Doc [here](https://nodejs.org/api/querystring.html).

4. So now we have the access_token and we're back at our app. What we can do is with that token go to CoLab API to get the user's name and all other information that you didn't know you could get. If you go to [APIDoc](https://apidocs.colab.duke.edu/) you'll see there's an identity API. When you try it out remember to flip the auth button next to that red exclamation mark or you'll FAIL. Anyways you can do a GET request to the api like this:

	```
	axios.get('https://api.colab.duke.edu/meta/v1/apis/identity/v1', {
		headers: {
			'x-api-key': client_id,
			'Authorization': `Bearer ${token}`
		}
	})
	```

	The 'x-api-key' is the same `client_id` you used for AppManager registration. `token` is the token you just got from the url parsing. Then the returned data is going to contain the juicy stuff, including name, year, etc.

5. There is no step 5.
