##Deploy your Code to Photon

---------------
### Step 1: Log in to your Photon Account.
Type `$ particle login` in your terminal/command line window and then follow the instruction to login to your account.

### Step 2: Deploy your Code
If you have only one file to deploy, just type `$ particle flash DeviceName FileName.ino`. Note this command is only useful when your file is in your working directory. If the file is not in your working directory, you can either use `$ cd` to go to the working directory or replace the name of your file with absolute file path. If your code is contained in serveral files, create a new folder and then put all the files (Include .h and .cpp library files) to that same folder, and then use `$ particle flash DeviceName path/folder` to deploy your code. The path/folder argument should be your folder's absolute file path.
## Tips:
If you need to transfer your photon to other users, be sure to go to [Particle Console](https://console.particle.io/) and deregister it. Otherwise, other users will not be able to claim this device.
If you have any other questions, please refer to [Particle CLI Docs](https://docs.particle.io/guide/tools-and-features/cli/photon/#particle-setup) for help.
