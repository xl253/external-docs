# How to enable Duke sign-in on your site

Use this guide to integrate OAuth sign-in to your web application, allowing your users to sign in with their Duke NetID and password.

## Register your app with the Co-Lab App Manager

Before you begin, visit [Co-Lab App Manager](https://appmanager.colab.duke.edu) to register your app.

Note your client ID and client secret, as you will need them to configure the OAuth client library.

## Choose a language specific library

The easiest way to integrate Duke NetID sign-in with OAuth is to have somebody else do it for you (surprise!).  Begin by looking for OAuth client libraries for your language.  If your favorite language is not listed below, please email us at [colab-ops@duke.edu](mailto:colab-ops@duke.edu) and we will look into libraries for your language.

Once your have identified your library, please use the specifications below:

* Authorize URL: `https://oauth.oit.duke.edu/oauth/authorize.php`
* Token URL: `https://oauth.oit.duke.edu/oauth/token.php`
* Resource URL: `https://oauth.oit.duke.edu/oauth/resource.php`

### Ruby on Rails

We recommend OmniAuth, and we have a Duke-specific gem for you.  You do not need to worry about configuring Duke OAuth URLs if you choose this option (however you still do need client ID and client secret).

In your `Gemfile`, add OmniAuth our gem:
```ruby
gem "omniauth", '~> 1.3.1'
gem "omniauth-duke-oauth2", :git => 'http://gitlab.oit.duke.edu/colab/omniauth-duke-oauth2.git', :ref => '5eaf6759'
```
NOTE: It is currently pinned to a specific Git commit.  Please check back soon for a new Gem specification that allows for non-breaking updates.

Please read [OmniAuth docs](https://github.com/omniauth/omniauth#getting-started) to learn more about initializer for OmniAuth, then modify or create `config/initializers/omniauth.rb`:
```ruby
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :duke_oauth2, "YOUR CLIENT ID HERE", "YOUR CLIENT SECRET HERE"
end
```

Finally, create a route for OmniAuth callback, and modify or create a controller action according to [OmniAuth documentation](https://github.com/omniauth/omniauth#integrating-omniauth-into-your-application).
