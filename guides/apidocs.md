# How to use Co-Lab APIs & App Manager

## Useful Links

  + [Co-Lab API Documentation](http://apidocs.colab.duke.edu)
  + [Co-Lab App Manager](https://appmanager.colab.duke.edu/)
  + [The OAuth Bible](http://oauthbible.com/)

## What to Do

  1. **Register your app at [Co-Lab App Manager](https://appmanager.colab.duke.edu/)**

    Click the link above, that will take you to our app manager system. Log in with your Duke NetID, then click on "Add an app" to the left.

    Now fill in the required fields.

      ![](/assets/apidocs1.png)

    In this example, we are testing at localhost, so I added a Redirect URI as `http://localhost:/3000`. In production, add your project server URI here.

    The next step is to add permissions to our project: in the example I want to retrieve all Co-Lab events, so I added "permission to read, create or edit events" here.

      ![](/assets/apidocs2.png)

    Finally, click submit and we're good to go!

  2. **Use the API**

    After clicking on submit, you will be taken to the App detail page as shown below

      ![](/assets/apidocs3.png)

    Now let's write some code to use the API!

    Open up your favorite text editor and paste the following:

    ```html
    <html>
    <head>
      <title>Co-Lab API test</title>
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    </head>
    <body>
      <h1>Co-Lab Events</h1>
      <ul id="events"></ul>
    </body>
    <script>
    const EVENTS_URL = "https://api.colab.duke.edu/events/v1/events";
    var events;

    var getAllEvents = function() {
      $.ajax({
        type: "GET",
        url: EVENTS_URL,
        headers: {
          "x-api-key": /*YOUR API KEY GOES HERE*/,
          "Content-Type": "application/json"
        },
        dataType: "json",
        contentType: 'application/json; charset=UTF-8'
      }).done(function(response) {
        events = response;
        console.log(events);
        displayEvents();
      }).fail(function(response) {
        console.error(response);
      });
    };

    var displayEvents = function() {
      events.map(function(item) {
        var $event = $("<li>");
        $event.text(item["title"] + " - " + item["description"]);
        $("#events").append($event);
      });
    };

    $(document).ready(getAllEvents);
    </script>
    </html>
    ```

    Save it as "test.html" and use your browser to open it:

      ![](/assets/apidocs4.png)

    You can see the list of events returned from the API!

    Since this is a GET API, it's really not that complicated - as long as you have the endpoint URL and send a GET request you'll get what you want. But what about other requests that require authentication?

  3. **Use the API - Expert**

    + Change your `test.html` to the following:

    ```html
    <html>
    <head>
      <title>Co-Lab API test</title>
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    </head>
    <body>
      <h1>Co-Lab Events</h1>
      <ul id="events"></ul>

      <h1>Post a New Event</h1>
      <span>New Event Title:</span>
      <input type="text" id="eventTitle"></input>
      <button id="post">Post!</button>
    </body>
    <script>
    const EVENTS_URL = "https://api.colab.duke.edu/events/v1/events";
    var events;

    var getAllEvents = function() {
      $.ajax({
        type: "GET",
        url: EVENTS_URL,
        headers: {
          "x-api-key": /*YOUR API KEY GOES HERE*/,
          "Content-Type": "application/json"
        },
        dataType: "json",
        contentType: 'application/json; charset=UTF-8'
      }).done(function(response) {
        events = response;
        console.log(events);
        displayEvents();
      }).fail(function(response) {
        console.error(response);
      });
    };

    var displayEvents = function() {
      $("#events").empty();
      events.map(function(item) {
        var $event = $("<li>");
        $event.text(item["title"] + " - " + item["description"]);
        $("#events").append($event);
      });
    };

    var eventData = {
      "title": "test event",
      "description": "test event",
      "facilitators": [
        "hy103"
      ],
      "occurrences": [
        {
          "description": "test occurrence",
          "capacity": 20,
          "enrollment_list": [
            {
              "net_id": "hy103",
              "attended": true,
              "waitlisted": false
            }
          ],
          "location": "TEC",
          "starttime": "2016-10-1 17:00:00",
          "endtime": "2016-10-1 19:00:00"
        }
      ]
    };
    var access_token = null;
    const OAUTH_URL = "https://oauth.oit.duke.edu/oauth/authorize.php";

    var postEvent = function() {
      eventData["title"] = $("#eventTitle").val();
    	$.ajax({
    		type: "POST",
    		url: "https://api.colab.duke.edu/events/v1/event",
    		headers: {
    			"x-api-key": "api-tester",
    			"Authorization": "Bearer " + access_token,
    			"Content-Type": "application/json"
    		},
    		dataType: "json",
    		contentType: 'application/json; charset=UTF-8',
    		data: JSON.stringify(eventData)
    	}).done(function(response) {
    		alert("Success!");
        getAllEvents();
    	}).fail(function(response) {
    		alert("Error!");
        console.error(response);
    	});
    };

    var getQueryString = function() {
    	return "?" + $.param({
    		response_type: 'token',
    		redirect_uri: 'http://localhost:3000/test.html',
    		client_id: 'api-tester',
    		scope: 'basic events:event:update',
    		state: 11291
    	});
    };

    var getAccessToken = function() {
    	var url = window.location.href;
    	var regex = new RegExp("access_token" + "(=([^&#]*)|&|#|$)"),
    	results = regex.exec(url);
    	if (results == null) return 0;
    	return results[2];
    };

    var checkAccessToken = function() {
    	access_token = getAccessToken();
    	console.log(access_token);
    	if (!access_token) {
    		window.location.replace(OAUTH_URL + getQueryString());
    	}
    };

    $(document).ready(function() {
      checkAccessToken();
      getAllEvents();
      $("#post").click(postEvent);
    });
    </script>
    </html>
    ```

    + add a redirect_uri as "http://localhost:3000/test.html"

    + install `npm serve` to serve your local machine directory:
      `npm install -g serve`

      If you don't have `npm` installed, read here: http://blog.npmjs.org/post/85484771375/how-to-install-npm

    + cd to your `test.html` directory and do `serve`. By default your folder will be visible at `http://localhost:3000`

    + open `test.html` with your browser, you'll first be directed to Duke netID login. After redirecting back you'll be able to post new events!

### Why does it work?

  You need an `access_token` for our API system. In `checkAccessToken` method, if there is currently no access_token stored, the page is directed to Duke OAuth login. After a successful login the access_token will be passed back in the url. We then decode the access_token in `getAccessToken` method to send it back to the server when we want to make an API call.
